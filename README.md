# LPF or Lubuntu Port Forwarding

This is a repo to clone to the Lubuntu live environment to test out port forwarding with the new router configs. This is confirmed working on Lubuntu 16.04.2 LTS.

## FUNCTION

1. Installs openssh-server
1. Changes SSH listening port to `61027`
1. Prompts to change `lubuntu`'s password to use with SSH
1. Starts a tiny web server with Python in the helloWorld directory to display a small `index.html` file

## USAGE

1. `sudo apt install git -y`
1. `git clone https://gitlab.com/oct8l/lpf.git`
1. `cd lpf/shellScripts`
1. `chmod +x start.sh`
1. `./start.sh`
1. Enter and confirm a password and press `Enter` to change `lubuntu`'s password
1. `CTRL+C` to exit the web server and get back to bash prompt