#!/bin/bash

echo "Let's get it started"

chmod +x installSshServer.sh \
    && chmod +x webServerQuick.sh \
    && chmod +x changepw.sh \
    && sh /home/lubuntu/lpf/shellScripts/installSshServer.sh \
    && sh /home/lubuntu/lpf/shellScripts/changepw.sh \
    && sh /home/lubuntu/lpf/shellScripts/webServerQuick.sh