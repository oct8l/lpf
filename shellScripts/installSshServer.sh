#!/bin/bash

sudo apt install openssh-server -y \
    && sudo cp ../Configs/sshd_config /etc/ssh/sshd_config \
    && sudo service ssh restart